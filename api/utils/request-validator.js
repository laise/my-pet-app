const { body } = require('express-validator');

const createUserValidator = [body('name', 'lastName', 'petName').isAlpha().withMessage('invalid value')];

const createUserSchema =  {
  name: {
    errorMessage: 'name cannot be empty',
    exists: true,
  },
  lastName: {
    errorMessage: 'lastName cannot be empty',
    exists: true,
  },
  petName: {
    errorMessage: 'petName cannot be empty',
    exists: true,
  },
};

module.exports = { createUserValidator, createUserSchema }
