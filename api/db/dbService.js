const { ObjectId, MongoClient } = require('mongodb');

const uri = "mongodb+srv://db-user:pwd-adm123@cluster0.exujo.mongodb.net/my-pet-app?retryWrites=true&w=majority";
const localUri = 'mongodb://localhost:27017/my-pet-app';
const databaseName = 'my-pet-app';

// Set up the connection to the db
const connectDb = async () => { 
  try {
    const client =  await MongoClient.connect(localUri, {useUnifiedTopology: true}); //connect using the string/uri to the mongo
    return client.db(databaseName); // connect to the db/colection
  } catch (err) {
    throw new Error('failed to connect to db ', err);
  }
}

const addUser = async userData => {
  const db = await connectDb();

  try {
    return await db.collection('pets-data').insertOne(userData);
  }
  catch (err) {
    console.log('failed to add user to the db: ', err);
  }
};

const deleteUsers = async userId => {
  const db = await connectDb();

  try {
    return await db.collection('pets-data').deleteOne({_id: ObjectId(userId)});
  }
  catch (err) {
    console.log('failed to delete user: ', err);
  }
};

const findUser = async userId => {
  const db = await connectDb();

  try {
    return db.collection('pets-data').find(ObjectId(userId)).toArray();
  }
  catch (err) {
    return console.log('failed to find user: ', err);
  }
}

const getUsers = async () => {
  const db = await connectDb();

  try {
    return db.collection('pets-data').find().toArray();
  }
  catch (err) {
    console.log('failed to load data from the db: ', err);
  }
};

const updateUser = async (userId, userNewData) => {
  const db = await connectDb();

  try {
    const dbOperation = await db.collection('pets-data').updateOne({_id: ObjectId(userId)}, { 
      $set: {
        name: userNewData.name,
        lastName: userNewData.lastName,
        petName: userNewData.petName
      }
    });
    return dbOperation;
  }
  catch (err) {
    console.log('failed to update user: ', err);
  }
};

module.exports = { addUser, deleteUsers, findUser, getUsers, updateUser };
