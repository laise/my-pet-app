const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')
const app = express();
const helmet = require('helmet');

const { validationResult, checkSchema } = require('express-validator');
const { addUser, deleteUsers, findUser, getUsers, updateUser } = require('./db/dbService');
const { createUserValidator, createUserSchema } = require('./utils/request-validator');

const port = process.env.PORT || 3001;

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json()); // for parsing application/json
app.use(cors());
app.use(helmet());

app.get('/', (request, response) => {
  response.status(200).send({
    version: '1.0.0'
  });
});

app.post('/users', checkSchema(createUserSchema), createUserValidator, async (request, response) => {
  const errors = validationResult(request);

  if (!errors.isEmpty()) {
    return response.status(422).send({ errors: errors.array() });
  } else {
    const result = await addUser(request.body);
    const userId = result.insertedId;
    if(result) {
      const body = await findUser(userId);
      response.status(200).send(body);
    }
  }
});

app.delete('/users/:id?', async (request, response) => {
  const userId = request.params.id;
  const result = await deleteUsers(userId);
  response.status(201).send(result);
});

app.get('/users', async (request, response) => {
  const users = await getUsers();

  if(users){
      response.status(200).send(users);
  }
});

app.get('/users/:id?', async (request, response) => {
  const userId = request.params.id;
  const result = await findUser(userId);

  if (result && result.length > 0) {
      response.status(200).send(result);
  } else {
      response.send('user not found');
  }
});

app.put('/users/:id?', async (request, response) => {
  const userId = request.params.id;
  const userNewData  = request.body;
  const result = await updateUser(userId, userNewData);
  response.status(201).send(result);
});
