# my-pet-app

learning node, express, react and mongo.
basic crud :)

### `cd api && npm run server`

Runs the api on [http://localhost:3001](http://localhost:3001)

### `cd web-app && npm start`

Starts the frontend app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
