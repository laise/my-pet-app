const API_ENDPOINT = 'http://localhost:3001';

export const addUser = (name, lastName, petName) => {
  return fetch(`${API_ENDPOINT}/users`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      name,
      lastName,
      petName,
    })
  })
};

export const getUsers = async () => {
  const response = await fetch(`${API_ENDPOINT}/users`, {
    method: 'GET',
  });
  return response.json();
};
