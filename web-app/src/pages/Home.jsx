import React from 'react';
import { useState } from 'react';
import { addUser } from '../services/users';

export default () => {
  const [name, setName] = useState('');
  const [lastName, setlastName] = useState('');
  const [petName, setPetName] = useState('');
  const [isUserCreated, setIsUserCreated] = useState(false);

  const handleOnSubmit = async (event) => {
    event.preventDefault();

    const response = await addUser(name, lastName, petName);

    if (response.status === 201) {
      setIsUserCreated(true);

      setTimeout(() => {
        setIsUserCreated(false);
        setName('');
        setlastName('');
        setPetName('');
      }, 3000);
    }
  };

  return (
    <div>
      <h2 className="text-center mt-4">Add pet</h2>
  
      <div className="row justify-content-md-center">
        <div className="col-6">
          <form onSubmit={handleOnSubmit}>
            <div className="form-group">
              <label htmlFor="name">Name</label>
              <input
                type="text"
                className="form-control"
                id="name"
                aria-describedby="name"
                placeholder="Type your name"
                value={name}
                onChange={event => setName(event.target.value)}
                required
              />
            </div>
            <div className="form-group">
              <label htmlFor="lastName">lastName</label>
              <input
                type="text"
                className="form-control"
                id="lastName"
                aria-describedby="lastName"
                placeholder="Type your lastName"
                value={lastName}
                onChange={event => setlastName(event.target.value)}
                required
              />
            </div>
            <div className="form-group">
              <label htmlFor="pet-name">Pet's name</label>
              <input
                type="text"
                className="form-control"
                id="pet-name"
                aria-describedby="pet-name"
                placeholder="Type your pet's name"
                value={petName}
                onChange={event => setPetName(event.target.value)}
                required
              />
            </div>
            <button
              type="submit"
              className="btn btn-primary"
            >
              Add my pet
            </button>
          </form>

          {isUserCreated && (
            <div className="alert alert-success mt-4" role="alert">
              Pet registered with success 🐶
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
