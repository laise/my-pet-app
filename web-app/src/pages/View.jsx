import React, { useState, useEffect } from 'react';
import { getUsers } from '../services/users';

export default () => {
  const loadPets = async () => {
    const users = await getUsers();
    setUsers(users);
  };
 
  const [users, setUsers] = useState([]);

  useEffect(() => {
    loadPets();
  }, []);

  return (
    <div>
      <h2 className="text-center mt-4"> Pets </h2>
      <table className="table table-striped table-dark">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">lastName</th>
            <th scope="col">Pet</th>
            <th scope="col">Id</th>
            <th scope="col">  </th>
          </tr>
        </thead>
        <tbody>
          {users.filter(user => user.name).map(user => {
            return (
              <tr key={user._id}>
                <td>{user.name}</td>
                <td>{user.lastName}</td>
                <td>{user.petName}</td>
                <td>{user._id}</td>
                <td><button type="button" class="btn btn-light">Edit Pet Data</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
