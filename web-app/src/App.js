import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import AboutUs from './pages/AboutUs'
import Home from './pages/Home'
import View from './pages/View'

class App extends React.Component {

  render() {
    return (
      <Router>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <span className="navbar-brand mb-0 h1">My Pet App 🐶</span>
          <ul className="nav justify-content-end">
            <li className="nav-item">
              <Link className="nav-link active" to="/">Add a new pet</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/pets">View pets</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/about">About Us</Link>
            </li>
          </ul>
        </nav>

        <div className="container">
          <Switch>
            <Route path="/" exact>
              <Home />
            </Route>
            <Route path="/pets" exact>
              <View />
            </Route>
            <Route path="/about" exact>
              <AboutUs />
            </Route>
          </Switch>
        </div>
      </Router>
    );
  }
}


export default App;
